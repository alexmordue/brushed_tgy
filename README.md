## NOTE ##

I cannot recommend this firmware. In testing it performs much more poorly than [MegaBrush](https://github.com/amcchord/MegaBrush), use that instead unless you have no other choice.

## Intro ##

Modified version of https://github.com/seandepagnier/brushed_tgy to work with "Kingkong 12A OPTO ESC BLHeli" Brushless ESCs from Hobbyking (currently £1.50 each, whoop!)

* Build it with `make bs_nfet.hex` (or just use the precompiled version in this repo)
* Upload  it with BLHeliSuite, via the BLHeli Bootloader provided on these boards (Use "Flash Other" function)

Note: Doesn't look like the ESCs have a thermistor. I inverted the direction of high temperature alert ADC reading. High temperature shut down wont work.

## Original Readme ##

This is brushed version of simonk motor controller for atmega8.

tested with afro nfet, and bs nfet esc

For blue series, bootloader is not loaded,
Solder to 6 pins on blueseries ESC, and connect to arduino
connect pins D10, D11, D12, D13, 5v, GND to breakout pads on bs esc
load arduino isp sketch to arduino
test programmer: (sometimes takes multiple tries)
  avrdude -c avrisp -b 19200 -P /dev/ttyUSB1 -u -p m8 -U flash:r:flash.hex:i -U eeprom:r:eeprom.hex:i
load brushless controller:
  avrdude -c avrisp -b 19200 -P /dev/ttyUSB1 -u -p m8 -U flash:w:bs_nfet.hex:i
1 wire bootloader is now programmed



For Afro ESC or bs ESC with afro bootloader
Load arduinoisplinker sketch to arduino
connect pin D2 to signal wire, 5v, and gnd
Load brushed controller:
"make upload_bs" or "make upload_afro" from tgy_brushed
